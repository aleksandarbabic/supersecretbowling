package bowling;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestBowling {
	Frame fr;
	BowlingGame bg;

	@Test
	public void testIfScoreIsProperlyReturned() throws BowlingException {
		fr = new Frame(3, 4);
		assertEquals(7, fr.score());
	}

	@Test
	public void testIsFrameIsStrike() throws BowlingException {
		fr = new Frame(10, 0);
		assertTrue(fr.isStrike());
	}

	@Test
	public void testIsFrameIsNotStrike() throws BowlingException {
		fr = new Frame(7, 0);
		assertFalse(fr.isStrike());
	}
	
	@Test(expected=BowlingException.class)
	public void testWhenScoreGoesHigherThan10InSingleFrame() throws BowlingException{
		fr = new Frame(6,6);
		
	}

	@Test
	public void testIsFrameIsSpare() throws BowlingException {
		fr = new Frame(5, 5);
		assertTrue(fr.isSpare());
	}

	@Test
	public void testIsFrameIsNotSpare() throws BowlingException {
		fr = new Frame(5, 2);
		assertFalse(fr.isSpare());
	}

	@Test
	public void testWhenFramesAreAdded() throws BowlingException {
		bg = new BowlingGame();
		helperAddAllFrames(bg, 10);
		assertEquals(10, bg.getFrames().size());
	}

	@Test(expected = BowlingException.class)
	public void testWhenFramesAreAddedException() throws BowlingException {
		bg = new BowlingGame();
		helperAddAllFrames(bg, 15);
	}

	@Test
	public void testBonusThrowsAtTheEndOfTheGame() throws BowlingException {
		bg = new BowlingGame();
		bg.setBonus(2, 4);
		boolean expectedResult = bg.getBonus().getFirstThrow() == 2 && bg.getBonus().getSecondThrow() == 4;
		assertTrue(expectedResult);
	}

	@Test
	public void testScoreWhenLastFrameIsSpare() throws BowlingException {
		bg = new BowlingGame();
		bg.addFrame(new Frame(1, 1));
		bg.addFrame(new Frame(1, 1));
		bg.addFrame(new Frame(1, 1));
		bg.addFrame(new Frame(1, 1));
		bg.addFrame(new Frame(1, 1));
		bg.addFrame(new Frame(1, 1));
		bg.addFrame(new Frame(1, 1));
		bg.addFrame(new Frame(1, 1));
		bg.addFrame(new Frame(1, 1));
		bg.addFrame(new Frame(5, 5));
		bg.setBonus(3, 3);
		assertEquals(31, bg.score());
	}

	@Test
	public void testScoreWhenLastFrameIsStrike() throws BowlingException {
		bg = new BowlingGame();
		bg.addFrame(new Frame(1, 1));
		bg.addFrame(new Frame(1, 1));
		bg.addFrame(new Frame(1, 1));
		bg.addFrame(new Frame(1, 1));
		bg.addFrame(new Frame(1, 1));
		bg.addFrame(new Frame(1, 1));
		bg.addFrame(new Frame(1, 1));
		bg.addFrame(new Frame(1, 1));
		bg.addFrame(new Frame(1, 1));
		bg.addFrame(new Frame(10, 0));
		bg.setBonus(3, 3);
		assertEquals(34, bg.score());
	}


	public void helperAddAllFrames(BowlingGame bg, int count) throws BowlingException {
		for (int i = 0; i < count; ++i) {
			bg.addFrame(new Frame((int) Math.random() * 5, (int) Math.random() * 5));
		}
	}
	
}
