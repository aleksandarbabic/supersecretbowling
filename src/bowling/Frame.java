package bowling;

public class Frame {
	private int firstThrow;
	private int secondThrow;
	private int frameScore = 0;

	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		frameScore = firstThrow + secondThrow;
		if(frameScore > 10)
			throw new BowlingException("Score out of boundaries, max is 10, but it is " + frameScore);
		this.firstThrow = firstThrow;
		this.secondThrow = secondThrow;
	}

	public int getFirstThrow() {
		return firstThrow;
	}

	public int getSecondThrow() {
		return secondThrow;
	}

	// returns the score of a single frame
	public int score(){
		frameScore = firstThrow + secondThrow;
		return frameScore;
	}

	// returns whether the frame is a strike or not
	public boolean isStrike() {

		return (firstThrow == 10) ? true : false;
	}

	// return whether a frame is a spare or not
	public boolean isSpare() {
		return firstThrow + secondThrow == 10 && !isStrike();
	}

	public void setFrameScore(int frameScore) {
		this.frameScore = frameScore;
	}
}
